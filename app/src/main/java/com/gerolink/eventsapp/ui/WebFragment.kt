package com.gerolink.eventsapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.CreateMethod
import by.kirich1409.viewbindingdelegate.viewBinding
import com.gerolink.eventsapp.BuildConfig
import com.gerolink.eventsapp.databinding.FragmentWebBinding

class WebFragment : Fragment() {

    private val binding: FragmentWebBinding by viewBinding(CreateMethod.INFLATE)

    private var url: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getArgs()
    }

    private fun getArgs() {
        arguments?.let {
            val safeArgs = WebFragmentArgs.fromBundle(it)
            url = BuildConfig.WEBSITE_URL + safeArgs.url
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        initWebView()
    }

    private fun initWebView() {
        binding.webView.apply {
            settings.javaScriptEnabled
            webViewClient = WebViewClient()
            webChromeClient = WebChromeClient()
        }
        url?.also { binding.webView.loadUrl(it) }
    }
}