package com.gerolink.eventsapp.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.gerolink.eventsapp.R
import com.gerolink.eventsapp.data.remote.response.models.EventResponseItem
import com.gerolink.eventsapp.databinding.ItemEventBinding
import com.gerolink.eventsapp.extensions.loadFromUrl

class EventsAdapter :
    RecyclerView.Adapter<EventsAdapter.EventsViewHolder>() {

    var items: MutableList<EventResponseItem> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val item = ItemEventBinding.inflate(layoutInflater, parent, false)
        return EventsViewHolder(item)
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class EventsViewHolder(private val itemBinding: ItemEventBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        private val context = itemBinding.root.context

        fun bind(item: EventResponseItem) {
            itemBinding.apply {
                name.text = item.actor.displayLogin
                repo.text = context.getString(R.string.event_repo_name, item.repo.name)
                time.text = context.getString(R.string.event_created_at, item.createdAt)
                type.text = item.type

                avatar.loadFromUrl(item.actor.avatarUrl, R.drawable.ic_avatar_placeholder)

                this.root.setOnClickListener { view ->
                    val action =
                        EventsFragmentDirections.actionEventsFragmentToWebFragment(item.repo.name)
                    view.findNavController().navigate(action)
                }
            }
        }
    }
}