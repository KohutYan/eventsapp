package com.gerolink.eventsapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.CreateMethod
import by.kirich1409.viewbindingdelegate.viewBinding
import com.gerolink.eventsapp.MarginItemDecoration
import com.gerolink.eventsapp.databinding.FragmentEventsBinding
import com.gerolink.eventsapp.extensions.fadeOutToGone
import com.gerolink.eventsapp.extensions.px
import com.gerolink.eventsapp.viewmodel.EventsViewModel

class EventsFragment : Fragment() {

    private val binding: FragmentEventsBinding by viewBinding(CreateMethod.INFLATE)
    private val viewModel: EventsViewModel by viewModels()

    private lateinit var eventsAdapter: EventsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUi()
        subscribeUi()
    }

    private fun initUi() {
        initList()
    }

    private fun initList() {
        eventsAdapter = EventsAdapter()
        binding.mainList.apply {
            adapter = eventsAdapter
            layoutManager = LinearLayoutManager(activity)
            addItemDecoration(MarginItemDecoration(DIVIDER_SIZE))
        }

        binding.shimmer.startShimmer()
    }

    private fun subscribeUi() {
        observeEvents()
    }

    private fun observeEvents() {
        viewModel.events.observe(viewLifecycleOwner, {
            eventsAdapter.items = it
            if (it.isEmpty().not())
            binding.shimmer.apply {
                stopShimmer()
                fadeOutToGone()
            }
        })
    }

    companion object {
        private val DIVIDER_SIZE = 8.px
    }
}