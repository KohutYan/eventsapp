package com.gerolink.eventsapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gerolink.eventsapp.data.remote.response.models.EventResponseItem
import com.gerolink.eventsapp.data.remote.retrofit.RetrofitFactory
import kotlinx.coroutines.launch
import retrofit2.HttpException

class EventsViewModel : ViewModel() {

    private val _events = MutableLiveData<ArrayList<EventResponseItem>>()
    val events: LiveData<ArrayList<EventResponseItem>> get() = _events

    init {
        getEvents()
    }

    private fun getEvents() {
        val service = RetrofitFactory.createRetrofitService()
        viewModelScope.launch {
            val response = service.getEvents(50, 1)
            try {
                if (response.isSuccessful) {
                    response.body().let {
                        _events.postValue(it)
                    }
                }
            } catch (e: HttpException) {
                Log.e("REQUEST", "Exception: $e")
            } catch (e: Throwable) {
                Log.e("REQUEST", "Something wrong: $e")
            }
        }
    }
}