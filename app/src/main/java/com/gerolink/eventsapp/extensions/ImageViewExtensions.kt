package com.gerolink.eventsapp.extensions

import android.widget.ImageView
import androidx.constraintlayout.widget.Placeholder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.gerolink.eventsapp.R

fun ImageView.loadFromUrl(url: String, placeholderId: Int = 0) {
    Glide.with(this)
        .load(url)
        .centerCrop()
        .transition(DrawableTransitionOptions.withCrossFade(100))
        .placeholder(placeholderId)
        .into(this)
}