package com.gerolink.eventsapp.data.remote.retrofit

import com.gerolink.eventsapp.data.remote.response.EventResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitApiService {

    @GET("/events")
    suspend fun getEvents(
        @Query("per_page") per_page:Int = 30,
        @Query("page") page: Int
    ): Response<EventResponse>

}