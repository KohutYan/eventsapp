package com.gerolink.eventsapp.data.remote.response.models

data class Repo(
    val id: Int,
    val name: String,
    val url: String
)