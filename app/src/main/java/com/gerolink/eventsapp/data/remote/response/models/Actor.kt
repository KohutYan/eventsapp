package com.gerolink.eventsapp.data.remote.response.models

import com.google.gson.annotations.SerializedName

data class Actor(
    @SerializedName("avatar_url")
    val avatarUrl: String,
    @SerializedName("display_login")
    val displayLogin: String,
    @SerializedName("gravatar_id")
    val gravatarId: String,
    val id: Int,
    val login: String,
    val url: String
)