package com.gerolink.eventsapp.data.remote.response.models

import com.google.gson.annotations.SerializedName

data class EventResponseItem(
    val actor: Actor,
    @SerializedName("created_at")
    val createdAt: String,
    val repo: Repo,
    val type: String
)