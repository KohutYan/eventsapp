package com.gerolink.eventsapp.data.remote.response

import com.gerolink.eventsapp.data.remote.response.models.EventResponseItem

class EventResponse : ArrayList<EventResponseItem>()