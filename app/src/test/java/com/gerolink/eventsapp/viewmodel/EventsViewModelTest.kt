package com.gerolink.eventsapp.viewmodel

import com.gerolink.eventsapp.data.remote.response.EventResponse
import com.gerolink.eventsapp.data.remote.retrofit.RetrofitApiService
import com.gerolink.eventsapp.data.remote.retrofit.RetrofitFactory
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class EventsViewModelTest {
    @Mock
    lateinit var mockedFactory: RetrofitFactory

    @Mock
    lateinit var mockedService: RetrofitApiService

    @Mock
    lateinit var mockedResponse: Response<EventResponse>
    lateinit var viewModel: EventsViewModel

    @Before
    fun before() {
        doReturn(mockedService).whenever(mockedFactory).createRetrofitService()
        CoroutineScope(Dispatchers.IO).launch {
            doReturn(mockedResponse).whenever(mockedService).getEvents(50, 1)
        }
        doReturn(true).whenever(mockedResponse).isSuccessful
        viewModel = EventsViewModel()
    }

    @Test
    fun service_should_return_response() {
        //given
        val service = mockedFactory.createRetrofitService()
        //when
        viewModel = EventsViewModel()
        //then
        CoroutineScope(Dispatchers.IO).launch {
            verify(service).getEvents(50, 1)
        }
    }

    @Test
    fun response_should_be_successful() {
        CoroutineScope(Dispatchers.IO).launch {
            //given
            val service = mockedFactory.createRetrofitService()
            val response = service.getEvents(50, 1)
            //when
            viewModel = EventsViewModel()
            //then
            val boolean = response.isSuccessful
            assert(boolean)
        }
    }
}